import React from 'react';
import './Burger.css';
import Salad from "../Salad/Salad";
import Cheese from "../Cheese/Cheese";
import Meat from "../Meat/Meat";
import Bacon from "../Bacon/Bacon";
import BreadTop from "../BreadTop/BreadTop";
import BreadBottom from "../BreadBottom/BreadBottom";



let Burger = (props)=> {
    let ingridientsObj = props.ingridients;
    let ingridients = Object.keys(ingridientsObj); // метод собирает в массив все ключи переданного объекта

    let finalArray = ingridients.map(ingridient => {
        let amount = ingridientsObj[ingridient].amount;
        let array = [];
        for(let i = 0; i < amount; i++) {
            switch (ingridient ){
                case 'salad':
                    array.push(<Salad/>);
                    break;
                case 'bacon':
                    array.push(<Bacon/>);
                    break;
                case 'meat':
                    array.push(<Meat/>);
                    break;
                case 'cheese':
                    array.push(<Cheese/>);
                    break;
            }
        }
        return array
    });
    console.log(finalArray);

    return(
        <div className="Burger">
            <BreadTop/>
            {finalArray}
            <BreadBottom/>
        </div>
    )
};
        export default Burger;